<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentsController extends Controller
{
    public function average(){

    	$average = Comment::avg('rate');

    	return $average;

    }

    public function index(){

    	$comments = Comment::orderBy('created_at', 'DESC')->paginate(3);

    	return $comments;

    }

    public function displayByRate(){

    	$comments = Comment::orderBy('rate', 'DESC')->paginate(3);

    	return $comments;
    }


    public function store(){

    	$validated = request()->validate([
    		'pseudo' => 'required|min:3|max:20',
    		'email' => 'required|email',
            'title' => 'required|min:3|max:20',
            'content' => 'required|min:3',
            'rate' => 'required|max:1'
        ]);

        return Comment::create($validated);

    }

    public function edit($id){

        $comment = Comment::find($id);

        return $comment;

    }

    public function update($id){

        $comment = Comment::find($id);

        $validated = request()->validate([
            'pseudo' => 'required|min:3|max:20',
            'email' => 'required|email',
            'title' => 'required|min:3|max:20',
            'content' => 'required|min:3',
            'rate' => 'required|max:1'

        ]);

        $comment->update($validated);

        return $comment;


    }

    public function destroy($id){

        $comment = Comment::find($id);

        if($comment->delete()){
            return true;
        }

        
    }

    
}
