<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/storeComment', 'CommentsController@store');
Route::get('/displayComments', 'CommentsController@index');
Route::delete('/supComment/{id}', 'CommentsController@destroy');
Route::get('/editComment/{id}', 'CommentsController@edit');
Route::patch('/updateComment/{id}', 'CommentsController@update');
