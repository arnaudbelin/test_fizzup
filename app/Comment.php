<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Comment extends Model
{
	
    protected $fillable = ['pseudo', 'email', 'title', 'content', 'rate'];
}
